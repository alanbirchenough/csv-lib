(ns csv-lib
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [clojure.string :as str]))

(defn read-csv
  [path]
  (with-open [reader (io/reader path)]
    (doall
      (csv/read-csv reader))))

(defn map->csv
  [map-data]
  (let [header (vec (apply clojure.set/union (map (comp set keys) map-data)))
        extract-fn (apply juxt header)
        body (mapv extract-fn map-data)]
    (vec (cons header body))))

(defn write-csv
  [file-path csv]
  (with-open [writer (io/writer file-path)]
    (csv/write-csv writer csv)))

(defn- strip-colon
  [[first-char & rest-chars :as whole-string]]
  (if (= first-char \:)
    (apply str rest-chars)
    whole-string))

(defn- string-as-keyword
  [string]
  (keyword (strip-colon string)))

(defn csv->map [csv-data]
  (map zipmap
       (->> (first csv-data) ;; First row is the header
            (mapv (comp string-as-keyword str/trim)) ;; Drop if you want string keys instead
            repeat)
       (map #(mapv str/trim %) (rest csv-data))))
